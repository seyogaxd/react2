import LogoImage from "../../../assets/logo/Logo.png"
import ShopIcon from "../../../assets/icons/shoped-icon.svg"
import FavoriteIcon from "../../../assets/icons/favorite-icon.svg"

export default function Header({ boughtItemsAmount, favoriteItemsAmount })
{
    return(
        <div className="header__container">
            <div className="header__wrapper">
                <div className="header__logo"><img src={LogoImage} alt="LogoImage" /></div>
                <div className="header__info">
                    <ul className="header__list">
                        <li className="header__list-item">
                            <div className="header__item-container">
                                <img src={ShopIcon} alt="Shoped-icon" />
                                <span className="header__item-number">
                                    {boughtItemsAmount}
                                </span>
                            </div>
                        </li>
                        <li className="header__list-item">
                            <div className="header__item-container">
                                <img src={FavoriteIcon} alt="Favorite-icon" />
                                <span className="header__item-number">
                                    {favoriteItemsAmount}
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            
        </div>
    )
}