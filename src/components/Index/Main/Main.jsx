import ShopList from "./ShopList/ShopList";

export default function Main(props)
{
    const {
        title,
        modalOpen,
        imageModalOpen,
        itemslist,
        addToShoped,
        addToFavorite,
        favoriteMassive,
        shopedMassive
    } = props;
    return(
        <div className="main__container">
            <h1>{title}</h1>
            <div className="main__container-items">
                <div className="main__items-list">
                <ShopList
                modalOpen={modalOpen}
                imageModalOpen={imageModalOpen}
                itemslist={itemslist}
                addToShoped={addToShoped}
                addToFavorite={addToFavorite}
                favoriteMassive={favoriteMassive}
                shopedMassive={shopedMassive}
                />
            </div>
            </div>
            
        </div>
    )
}