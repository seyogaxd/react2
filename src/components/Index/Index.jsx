import Header from "./Header/Header"
import Main from "./Main/Main"
import PropTypes from "prop-types";
import './Styles/IndexStyles.scss'
import { useState, useEffect } from "react";
import ModalText from "../Modal/ModalTypes/ModalText"

export default function IndexPage()
{   
    const [items, setItems] = useState([]);



    const sendRequest = async (url) => {
        const response = await fetch(url);
        const items = await response.json();
        return items;
    }
        

    useEffect(() => {
        sendRequest("items.json").then((items) => {
            setItems(items);
        });
    }, 
    []
    );



    const [ShopItems, setShopItems] = useState(
        JSON.parse(localStorage.getItem("ShopItems")) || []
    );


    const [FavoriteItems, setFavoriteItems] = useState(
        JSON.parse(localStorage.getItem("FavoriteItems")) || []
    );


    

    //localStorage.clear();

    const [isModalOpen, setModalOpen] = useState(false)
    const [isImageModalOpen, setImageOpen] = useState(false)
      
    const [modalItemId, setModalItemId] = useState(null);

    const openModal = (itemId) => {
        setModalItemId(itemId); 
        setModalOpen(true);
    };
    const openImageModal=()=>setImageOpen(true)
      
    
    const closeModal=()=>setModalOpen(false)
    const closeImageModal=()=>setImageOpen(false)

   // const addToFavorite=(item)=>{
   //     setFavoriteItems()
   // }
    const addToShoped = (itemToAdd) => {
        itemToAdd = items[modalItemId-1];
    
        const newItem = {
            id: itemToAdd.id,
            name: itemToAdd.name,
            price: itemToAdd.price,
            image: itemToAdd.image,
            article: itemToAdd.article,
        };
        const newShopedItems = [...ShopItems, newItem];

        setShopItems(newShopedItems);
        localStorage.setItem(
            "ShopItems",
            JSON.stringify(newShopedItems)
        );
        closeModal();
       
};

    const addToFavorite = (itemToAdd) => {
        const itemIdx = FavoriteItems.findIndex(
            (item) => item.id === itemToAdd.id
        );
  
        if (itemIdx === -1) {
            const newItem = {
                id: itemToAdd.id,
                name: itemToAdd.name,
                price: itemToAdd.price,
                image: itemToAdd.image,
                article: itemToAdd.article,
                color: itemToAdd.color,
            };
            
            const newFavoriteItems = [...FavoriteItems, newItem];
  
            setFavoriteItems(newFavoriteItems);

            localStorage.setItem(
                "FavoriteItems",
                JSON.stringify(newFavoriteItems)
            );
           
        } 
        else {
            const newFavoriteItems = FavoriteItems.filter(
                (item) => item.id !== itemToAdd.id
            );
            
            setFavoriteItems(newFavoriteItems);

            localStorage.setItem(
                "FavoriteItems",
                JSON.stringify(newFavoriteItems)
            );
        }
    };

    return(
        <>
        <Header
        boughtItemsAmount={ShopItems.length}
        favoriteItemsAmount={FavoriteItems.length}
        />
        <Main
        modalOpen={openModal}
        imageModalOpen={openImageModal}
        addToFavorite={addToFavorite}
        itemslist={items}
        favoriteMassive={FavoriteItems}
        shopedMassive={ShopItems}
        />
         {isModalOpen&&(
            <ModalText
            title={'Buy Product "NAME"'}
            text={'Description for you product'}
            closeWindow={closeModal}
            onClick={addToShoped}
            />
         )

         }   
        </>
    )
}