import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody";
import ModalClose from "../ModalClose/ModalClose";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import Modal from "../Modal";

export default function ModalText({ className, title, text, closeWindow, onClick })
{
    return(
        <ModalWrapper closeWindow={closeWindow}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={closeWindow}></ModalClose>
                </ModalHeader>
                <ModalBody>
                        <h2>{title}</h2>
                        <p>{text}</p>
                </ModalBody>
                <ModalFooter
                firstText={"ADD TO BOUGHTED"}
                firstClick={onClick}
                >
                </ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}