import { useState } from 'react'

import './App.css'
import Button from './components/Button/Button'
import ModalImage from './components/Modal/ModalTypes/ModalImage'
import ModalText from './components/Modal/ModalTypes/ModalText'

import Index from './components/Index/Index'


function App() {
  

  return (
    <>
     <Index></Index>
    </>
  )
}

export default App
